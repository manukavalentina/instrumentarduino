# Projet ISN 2018: musiquArduino
### Auteurs: Lou DE CARNE, Manuka STRATTA, Ines VERHOOSEL

# Description du projet
musiquArduino est un projet qui relie les domaines de la musique, l'électronique et la programmation.
Le projet est divisé en trois parties principales:
  - L'instrument Arduino
  - L'interface Tkinter
  - Les deux sites web

# Remarques importantes

Le script de l'interface fonctionne avec Python 3.6.4.
Cependant, les scripts ne sont **pas exécutables** sans la carte Arduino, branchée à l'ordinateur. Puisque le groupe n'a pas pu fournir plusieurs versions de la cartre Arduino et les composants nécessaires, nos scripts ne peuvent pas être exécutés directement.
Cependant, tout notre code est entièrement disponible pour consultation. Nous fournissons aussi des **vidéos explicatives** montrant chaque aspect de notre projet en exécution. 

## Organisation des dossiers
Nos scripts (de l’interface, de l'instrument Arduino et des sites web) se trouvent dans le dossier **“programmes”** et les sous-dossiers correspondants.
Six vidéos illustrant les différentes parties et fonctionnalités de notre projet se trouvent dans le dossier **“videos-explicatives”**.
Dans **“annexes”**, vous trouverez une version pdf de notre diagramme de Gantt ainsi que notre sitographie.

# Licence du projet
CC BY-NC-SA: Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions
Sous ces conditions, un individu est autorisé à :
Partager — copier, distribuer et communiquer le matériel par tous moyens et sous tous formats
Adapter — remixer, transformer et créer à partir du matériel
Pour plus d’informations, veuillez visiter le site web de Creative Commons suivant : 
https://creativecommons.org/licenses/by-nc-sa/3.0/deed.fr

# Crédits
Les images utilisées dans notre interface proviennent du site www.pexels.com. Puisqu'elles ont une licence CC0, notre projet est légalement réalisable dans le domaine public.
De même, les sons utilisés pour le site web “percussion” proviennent du site www.freesound.org et ont une licence CC0.

> "Notre outil CC0 permet aux titulaires
> de droits de renoncer à tous leurs droits
> et de placer une œuvre dans le domaine public"
– informations provenant du site officiel Creative Commons



