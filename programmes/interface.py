#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Interface
#     Licence : CC BY-NC-SA
#     Auteurs : DECARNE Lou, STRATTA Manuka, VERHOOSEL Ines

from tkinter import *
from PIL import Image, ImageTk
import webbrowser   # Importation de la librairie qui permet de renvoyer l'utilisateur vers un site web
import os           # Importation de la librairie qui permet de recuperer les chemins relatifs et absolus
import serial       # Importation de la librairie qui permet la communication serie, pySerial
import time         # Importation de la librairie qui permet les delais, pauses

arduino = serial.Serial('/dev/cu.usbmodem1421', 9600)   # Recuperation du port série Arduino
time.sleep(1) # Attente de l'initialisation du port serie...


# Recuperation des chemins 
relPathJacques = os.path.abspath("../photos/frerejacques.png")
relPathTwinkle = os.path.abspath("../photos/twinkle.png")
relPathJingle = os.path.abspath("../photos/jingle.png")
relPathWallpaper = os.path.abspath("../photos/wallpaper.jpg")
relPathNote = os.path.abspath("../photos/note.png")
relPathCleDeSol = os.path.abspath("../photos/cleDeSol.jpeg")
relPathCheck = os.path.abspath("../photos/check.jpg")
relPathBatterie = os.path.abspath("../photos/batterie.jpg")
relPathPartition = os.path.abspath("../photos/partition.jpg")
relPathQCM = os.path.abspath("siteQCM/QCM.html")
relPathPercussions = os.path.abspath("sitePercussions/sitePercussions.html")

def listeMusiques():
    listeFenetre = Toplevel()

    jacquesImage = Image.open(relPathJacques)
    photoJacques = ImageTk.PhotoImage(jacquesImage) #la variable photo contient une image tkinter de l'image modifiee

    twinkleImage = Image.open(relPathTwinkle)
    photoTwinkle = ImageTk.PhotoImage(twinkleImage)

    jingleImage = Image.open(relPathJingle)
    photoJingle = ImageTk.PhotoImage(jingleImage)

    #creation d'une grid
    listeFenetre.columnconfigure(0, weight = 1)
    listeFenetre.columnconfigure(1, weight = 1)
    listeFenetre.columnconfigure(2, weight = 1)
    listeFenetre.rowconfigure(0, weight = 1)
    listeFenetre.rowconfigure(1, weight = 1)

    #on place l'image dans un label
    labelJacques = Label(listeFenetre, image = photoJacques)
    labelJacques.image = photoJacques
    labelJacques.grid(row = 1, column = 0)
    labelTwinkle = Label (listeFenetre, image = photoTwinkle)
    labelTwinkle.image = photoTwinkle
    labelTwinkle.grid (row = 1, column = 1)
    labelJingle = Label (listeFenetre, image = photoJingle)
    labelJingle.image = photoJingle
    labelJingle.grid (row = 1, column = 2)

    #creation de labels texte
    labelTexteJ = Label(listeFenetre, text= "Frère jacques")
    labelTexteJ.grid(row = 0, column = 0)
    labelTexteT = Label(listeFenetre, text= "Twinkle Twinkle Little Star")
    labelTexteT.grid(row = 0, column = 1)
    labelTexteJ = Label(listeFenetre, text= "Jingle Bells")
    labelTexteJ.grid(row = 0, column = 2)


# Definiton des fonctions pour lancer les sites web depuis l'interface
def ouvrirPageHTML():
    webbrowser.open("file://" + os.path.realpath(relPathQCM))

def ouvrirPercussionsHTML():
    webbrowser.open("file://" + os.path.realpath(relPathPercussions))


# Definition des fonctions pour la communication serie ordinateur-Arduino
def jouerDo():
	arduino.write(b'c')    # On envoie le caractere "c" au port serie de l'Arduino. 'b' pour convertir en 'bytes'.
	time.sleep(0.5)

def jouerRe():
	arduino.write(b'd')
	time.sleep(0.5)

def jouerMi():
	arduino.write(b'e')
	time.sleep(0.5)

def jouerFa():
	arduino.write(b'f')
	time.sleep(0.5)

def jouerSol():
	arduino.write(b'g')
	time.sleep(0.5)

def jouerLa():
	arduino.write(b'h')
	time.sleep(0.5)

def jouerSi():
	arduino.write(b'i')
	time.sleep(0.5)

def jouerDO():
	arduino.write(b'j')
	time.sleep(0.5)


def notes():
    #creation d'une fenetre pop up
    notesFenetre = Toplevel()

    #creation d'une grid
    notesFenetre.columnconfigure(0, weight = 1)
    notesFenetre.columnconfigure(1, weight = 1)
    notesFenetre.columnconfigure(2, weight = 1)
    notesFenetre.columnconfigure(3, weight = 1)
    notesFenetre.columnconfigure(4, weight = 1)
    notesFenetre.columnconfigure(5, weight = 1)
    notesFenetre.columnconfigure(6, weight = 1)
    notesFenetre.columnconfigure(7, weight = 1)

    notesFenetre.rowconfigure(0, weight = 1)
    notesFenetre.rowconfigure(1, weight = 1)

    #ouverture de l'image utilisée et transformation en photo utilisable par tkinter
    global photoNotes
    notesImage = Image.open(relPathNote)
    photoNotes = ImageTk.PhotoImage(notesImage)

    #creation de boutons
    doButton = Button(notesFenetre, image = photoNotes, command=jouerDo)
    doButton.grid( row = 0, column = 0)

    reButton = Button(notesFenetre, image = photoNotes, command=jouerRe)
    reButton.grid( row = 0, column = 1)

    miButton = Button(notesFenetre, image = photoNotes, command=jouerMi)
    miButton.grid( row = 0, column = 2)

    faButton = Button(notesFenetre, image = photoNotes, command=jouerFa)
    faButton.grid( row = 0, column = 3)

    solButton = Button(notesFenetre, image = photoNotes, command=jouerSol)
    solButton.grid( row = 0, column = 4)

    laButton = Button(notesFenetre, image = photoNotes, command=jouerLa)
    laButton.grid( row = 0, column = 5)

    siButton = Button(notesFenetre, image = photoNotes, command=jouerSi)
    siButton.grid( row = 0, column = 6)

    DOButton = Button(notesFenetre, image = photoNotes, command=jouerDO)
    DOButton.grid( row = 0, column = 7)

    #creation de labels
    labelDo = Label (notesFenetre, text = "do")
    labelDo.grid (row = 1, column = 0)

    labelRe = Label (notesFenetre, text = "ré")
    labelRe.grid (row = 1, column = 1)

    labelMi = Label (notesFenetre, text = "mi")
    labelMi.grid (row = 1, column = 2)

    labelFa = Label (notesFenetre, text = "fa")
    labelFa.grid (row = 1, column = 3)

    labelSol = Label (notesFenetre, text = "sol")
    labelSol.grid (row = 1, column = 4)

    labelLa = Label (notesFenetre, text = "la")
    labelLa.grid (row = 1, column = 5)

    labelSi = Label (notesFenetre, text = "si")
    labelSi.grid (row = 1, column = 6)

    labelDO = Label (notesFenetre, text = "do aigu")
    labelDO.grid (row = 1, column = 7)



#Creation et configuration de la fenetre principale
root = Tk()
root.title("musiquArduino")

hauteurFenetre = str(root.winfo_screenheight()) #On recupere les dimensions de l'ecran
largeurFenetre = str(root.winfo_screenwidth())
root.geometry(largeurFenetre+"x"+hauteurFenetre) #exemple: largeur de 200, hauteur de 100

#Creation de la grid principale
root.columnconfigure(0, weight = 1, pad = 5)
root.columnconfigure(1, weight = 1, pad = 5)
root.columnconfigure(2, weight= 1, pad = 5)
root.columnconfigure(3, weight = 1, pad = 5)
root.rowconfigure(0, weight = 1, pad = 5)

backgroundImage = Image.open(relPathWallpaper)
photoBackground = ImageTk.PhotoImage(backgroundImage)   #Insertion d'une photo en fond d'ecran
labelBackground = Label(root, image = photoBackground)
labelBackground.place(x=0, y=0, relwidth=1, relheight=1)

#creation des boutons de la fenetre principale
partitionImage = Image.open(relPathPartition)
photoPartition = ImageTk.PhotoImage(partitionImage)
listeButton = Button(root, compound=TOP, bg = "black", text = "Liste de musiques que tu peux jouer!", image = photoPartition, command = listeMusiques)
listeButton.grid(row = 0, column = 0)

photoCleDeCol = Image.open(relPathCleDeSol)
photoCleDeCol = ImageTk.PhotoImage(photoCleDeCol)
jouerNotesButton = Button(root, compound=TOP, text = "jouer les notes", image = photoCleDeCol, command = notes)
jouerNotesButton.grid(row = 0, column = 3)

checkImage = Image.open(relPathCheck)
photoCheck = ImageTk.PhotoImage(checkImage)
QCMButton = Button(root, compound=TOP, text= "QCM", image = photoCheck, command = ouvrirPageHTML)
QCMButton.grid(row = 0, column = 1)

batterieImage = Image.open(relPathBatterie)
photoBatterie = ImageTk.PhotoImage(batterieImage)
percussionButton= Button(root, compound=TOP, text= "percussions!", image = photoBatterie, command = ouvrirPercussionsHTML)
percussionButton.grid(row = 0, column = 2)



root.mainloop()
