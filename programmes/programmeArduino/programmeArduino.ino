/*Programme Arduino
Licence : CC BY-NC-SA
Auteurs : DECARNE Lou, STRATTA Manuka, VERHOOSEL Ines
*/

// On fixe les numéros des pins de chacun des composants. Ex: le buzzer piezo a pour numéro de pin 4
int piezo = 4;
int bouton = 8;
int emetteur = 12;
int recepteur = 11;
int emetteur2 = 7;
int recepteur2 = 6;
int emetteur3 = 41;
int recepteur3 = 40;
int emetteur4 = 37;
int recepteur4 = 36;
int emetteur5 = 39;
int recepteur5 = 38;

// Chaque liste contient les fréquences des notes de Do à Do de l'octave supérieure. Ex: le do de l'octave inférieure a pour fréquence 261 Hz
int octaveBasse[] = {261,294,329,349,392,440,493,523};
int octaveHaute[] = {523,587,659,698,784,880,987,1047};

// Définition des autres variables, non-fixées.
int note;
int octave=0;
int etatBouton=0;
int etatBoutonAnterieur=0;

void setup() {
  // Initialisation des composants: ce code ne sera exécuté qu'une seule fois au début.
  pinMode(piezo,OUTPUT);
  pinMode(bouton,INPUT);
  pinMode(emetteur, OUTPUT);
  pinMode(recepteur, INPUT);
  pinMode(emetteur2, OUTPUT);
  pinMode(recepteur2, INPUT);
  pinMode(emetteur3, OUTPUT);
  pinMode(recepteur3, INPUT);
  pinMode(emetteur4, OUTPUT);
  pinMode(recepteur4, INPUT);
  pinMode(emetteur5, OUTPUT);
  pinMode(recepteur5, INPUT);

  // Commencement de la communication avec le port série
  Serial.begin(9600);
  
}


void loop() {


  // 1– Mesure de la distance objet-capteur1

  // Définition des variables
  float temps, distance;
  
  // Mesure du temps entre l'emission et la reception de l'onde ultrasonore
  digitalWrite(emetteur, LOW);
  delayMicroseconds(2);         // Un petit délai est nécessaire pour laisser le temps à l'onde de faire l'aller-retour
  digitalWrite(emetteur, HIGH);
  delayMicroseconds(2);
  digitalWrite(emetteur, LOW);
  
  // Calcul de la distance et conversion en cm
  temps = pulseIn(recepteur, HIGH);
  distance = (temps * 0.034) / 2;   // 340m/s = 0.034cm/µs (vitesse du son dans l'air) ; division par 2 car l'onde fait l'aller-retour


  // 2– Mesure de la distance objet-capteur2
  float temps2, distance2;

  // Mesure du temps entre l'emission et la reception de l'onde
  digitalWrite(emetteur2, LOW);
  delayMicroseconds(2);
  digitalWrite(emetteur2, HIGH);
  delayMicroseconds(2);
  digitalWrite(emetteur2, LOW);

  // Calcul de la distance et conversion en cm
  temps2 = pulseIn(recepteur2, HIGH);
  distance2 = (temps2 * 0.034) / 2;


  // 3– Mesure de la distance objet-capteur3
  float temps3, distance3;
  
  // Mesure du temps entre l'emission et la reception de l'onde
  digitalWrite(emetteur3, LOW);
  delayMicroseconds(2);
  digitalWrite(emetteur3, HIGH);
  delayMicroseconds(2);
  digitalWrite(emetteur3, LOW);
  
  // Calcul de la distance et conversion en cm
  temps3 = pulseIn(recepteur3, HIGH);
  distance3 = (temps3 * 0.034) / 2;


  // 4– Mesure de la distance objet-capteur4
  float temps4, distance4;
  
  // Mesure du temps entre l'emission et la reception de l'onde
  digitalWrite(emetteur4, LOW);
  delayMicroseconds(2);
  digitalWrite(emetteur4, HIGH);
  delayMicroseconds(2);
  digitalWrite(emetteur4, LOW);
  
  // Calcul de la distance et conversion en cm
  temps4 = pulseIn(recepteur4, HIGH);
  distance4 = (temps4 * 0.034) / 2;


  // 5– Mesure de la distance objet-capteur5
  float temps5, distance5;
  
  // Mesure du temps entre l'emission et la reception de l'onde
  digitalWrite(emetteur5, LOW);
  delayMicroseconds(2);
  digitalWrite(emetteur5, HIGH);
  delayMicroseconds(2);
  digitalWrite(emetteur5, LOW);
  
  // Calcul de la distance et conversion en cm
  temps5 = pulseIn(recepteur5, HIGH);
  distance5 = (temps5 * 0.034) / 2;


  // Lecture de l'état du bouton: valeur de 1 (bouton appuyé) ou 0 
  etatBouton = digitalRead(bouton);

  // Si le bouton est appuyé ET qu'il ne l'était pas avant, changer la valeur de la variable "octave" de 0 à 1 (ou vice-versa)
  if (etatBouton == HIGH and etatBoutonAnterieur == LOW) {     
    if (octave==0) {
      octave = 1;
      delay(200);
    }
    else {
      octave = 0;
      delay(200);
    }
  } 
  
  // On empêche la valeur de "octave" de changer 2 fois (ex: de 0 à 1 puis de nouveau à 0) dans le cas où le bouton reste enfoncé trop longtemps
  etatBoutonAnterieur = etatBouton; 


  // Buzzer joue la note DO si distance objet-capteur1 est inférieure à 10cm
  if (distance<10) {

    if (octave == 0) {        // si la valeur de "octave" est 0, jouer la note de l'octave basse
      note = octaveBasse[0];  // l'indice [0] correspond à la première valeur de la liste octaveBasse: c'est la fréquence du do
      tone(piezo,note,500);   // jouer la note pendant une durée de 500ms
      delay(50);
    }
    
    if (octave != 0) {        // si la valeur de "octave" n'est pas 0 (donc 1), jouer la note de l'octave haute
      note = octaveHaute[0];
      tone(piezo,note,500);
      delay(50);
    }

  }

  // Buzzer joue la note RE si distance objet-capteur2 est inférieure à 10cm
  if (distance2<10) {

    if (octave == 0) {  
      note = octaveBasse[1];
      tone(piezo,note,500);
      delay(50);
    }
    
    if (octave != 0) {  
      note = octaveHaute[1];
      tone(piezo,note,500);
      delay(50);
    }
  
  }

  // Buzzer joue la note MI si distance objet-capteur3 est inférieure à 10cm
  if (distance3<10) {

    if (octave == 0) {  
      note = octaveBasse[2];
      tone(piezo,note,500);
      delay(50);
    }
    
    if (octave != 0) {  
      note = octaveHaute[2];
      tone(piezo,note,500);
      delay(50);
    }
  
  }

  // Buzzer joue la note FA si distance objet-capteur4 est inférieure à 10cm
  if (distance4<10) {

    if (octave == 0) {  
      note = octaveBasse[3];
      tone(piezo,note,500);
      delay(50);
    }
    
    if (octave != 0) {  
      note = octaveHaute[3];
      tone(piezo,note,500);
      delay(50);
    }
  
  }

  // Buzzer joue la note SOL si distance objet-capteur5 est inférieure à 10cm
  if (distance5<10) {

    if (octave == 0) { 
      note = octaveBasse[4];
      tone(piezo,note,500);
      delay(50);
    }
    
    if (octave != 0) {  
      note = octaveHaute[4];
      tone(piezo,note,500);
      delay(50);
    }
  
  }


  if (Serial.available()) {                 // Vérifie si le port série est disponible
      char ecouteSerial = Serial.read();    // Ecoute / lecture du contenu du port série (informations envoyées au port depuis le script Python)
      
      if (ecouteSerial == 'c') {            // Si le caractère 'c' est présent dans le port série, jouer la note DO
          tone(piezo,261,500);
          delay(50);
      }
      else if (ecouteSerial == 'd') {       // Si le caractère 'd' est présent dans le port série, jouer la note RE
          tone(piezo,294,500);
          delay(50);
      }
      else if (ecouteSerial == 'e') {       // Si le caractère 'e' est présent dans le port série, jouer la note MI
          tone(piezo,329,500);
          delay(50);
      }
      else if (ecouteSerial == 'f') {       // Si le caractère 'f' est présent dans le port série, jouer la note FA
          tone(piezo,349,500);
          delay(50);
      }
      else if (ecouteSerial == 'g') {       // Si le caractère 'g' est présent dans le port série, jouer la note SOL
          tone(piezo,392,500);
          delay(50);
      }
      else if (ecouteSerial == 'h') {       // Si le caractère 'h' est présent dans le port série, jouer la note LA
          tone(piezo,440,500);
          delay(50);
      }
      else if (ecouteSerial == 'i') {       // Si le caractère 'i' est présent dans le port série, jouer la note SI
          tone(piezo,493,500);
          delay(50);
      }
      else if (ecouteSerial == 'j') {       // Si le caractère 'j' est présent dans le port série, jouer la note DO aigu
          tone(piezo,523,500);
          delay(50);
      }                     

  }


}
