
document.addEventListener('keydown', function(touche) {   // On écoute, on attend que l'utilisateur active une touche du clavier

    if (touche.keyCode == 65) {                   // Si la touche de code "65" (le "A") est activée, lancer "son1" (boom.wav)
      document.getElementById('son1').play();
    }
    if (touche.keyCode == 83) {
      document.getElementById('son2').play();
    }
    if (touche.keyCode == 68) {
      document.getElementById('son3').play();
    }
    if (touche.keyCode == 70) {
      document.getElementById('son4').play();
    }
    if (touche.keyCode == 71) {
      document.getElementById('son5').play();
    }
    if (touche.keyCode == 72) {
      document.getElementById('son6').play();
    }
    if (touche.keyCode == 74) {
      document.getElementById('son7').play();
    }
    if (touche.keyCode == 75) {
      document.getElementById('son8').play();
    }
    if (touche.keyCode == 76) {
      document.getElementById('son9').play();
    }

});
