// Creation d'une fonction qui permet d'afficher le texte quand la checkbox est clickée
function montrerLeTexte(checkbox,texte)
{
   if( checkbox.checked == true )
   {
      document.getElementById(texte).style.display = "block"; // Si checkbox clickée, afficher texte
   }
   else
   {
      document.getElementById(texte).style.display = "none";  // Sinon, le laisser caché
   }
}
